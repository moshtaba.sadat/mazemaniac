package mazemaniac;

import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.loadingscreen.LoadingScreenPresenter;
import mazemaniac.view.loadingscreen.LoadingScreenView;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;

import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.Objects;


import static mazemaniac.view.SceneUtils.exit;
import static mazemaniac.view.SceneUtils.imageFitter;

public class Main extends Application {

    @Override
    public void start(Stage stage){



        LoadingScreenView view = new LoadingScreenView();
        GameManager model = new GameManager();

        new LoadingScreenPresenter(view,model);

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                exit(stage,event,"Are you sure?","Pls stay");
            }
        });

        imageFitter(view.getSplashArtImage(),stage);

        Scene scene = new Scene(view,1024,800);
        scene.getStylesheets().add("Style.css");
        stage.setTitle("Maze Maniac");
        stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/LoadingScreen/SplashArt.png"))));
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }


}