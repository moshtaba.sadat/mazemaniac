package mazemaniac.view;

import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.mainmenu.MainMenuPresenter;
import mazemaniac.view.mainmenu.MainMenuView;
import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.Optional;

public class SceneUtils {

    public static FadeTransition fadeAnimation(Node node, double toValue, double seconds) {

        FadeTransition fadeOut  = new FadeTransition(Duration.seconds(seconds), node);
        fadeOut.setToValue(toValue);

        return fadeOut;

    }

    public static TranslateTransition moveToAnimation(Node node, double toPosition, char axis) {
        TranslateTransition move = new TranslateTransition(Duration.seconds(0.5), node);

        if (axis == 'y') {
            move.setByY(toPosition);
        } else if (axis == 'x') {
            move.setByX(toPosition);
        }
        return move;
    }

    public static void onHoverOnReleaseAnimation(Node node,double fromXY,double toXY){
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(100),node);

        scaleTransition.setFromX(fromXY);
        scaleTransition.setFromY(fromXY);

        scaleTransition.setToX(toXY);
        scaleTransition.setToY(toXY);

        scaleTransition.play();

    }

    public static void hoverAnimation(Button button){

        button.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                onHoverOnReleaseAnimation(button,1.0,1.1);
            }
        });

        button.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                onHoverOnReleaseAnimation(button,1.1,1.0);
            }
        });

    }

    public static void imageFitter(ImageView image, Stage stage){
        image.fitHeightProperty().bind(stage.heightProperty());
        image.fitWidthProperty().bind(stage.widthProperty());
    }

    public static void toMainMenu(Button button, GameManager model){

        MainMenuView mainMenuView = new MainMenuView();
        new MainMenuPresenter(mainMenuView, model);

        button.getScene().setRoot(mainMenuView);

        Stage stage = (Stage) mainMenuView.getScene().getWindow();
        imageFitter(mainMenuView.getSplashArtImage(),stage);

        fadeAnimation(mainMenuView.getScene().getRoot(), 1, 0.3).play();

    }

    public  static  void exit(Stage stage, Event event,String headerText,String contentText){

        final Alert Quit = new Alert(Alert.AlertType.CONFIRMATION);

        Quit.setHeaderText(headerText);
        Quit.setContentText(contentText);
        Optional<ButtonType> choice = Quit.showAndWait();

        if (choice.get().getText().equalsIgnoreCase("CANCEL")) {
            event.consume();
        }
        else {
            stage.close();
        }
    }














}
