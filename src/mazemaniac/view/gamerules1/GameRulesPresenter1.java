package mazemaniac.view.gamerules1;

import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.gamerules2.GameRulesPresenter2;
import mazemaniac.view.gamerules2.GameRulesView2;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

import static mazemaniac.view.SceneUtils.*;

public class GameRulesPresenter1 {

    private GameRulesView1 view;

    private GameManager model;

    public GameRulesPresenter1(GameRulesView1 view, GameManager model) {
        this.view = view;
        this.model = model;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers(){
        view.getTakeBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                view.getTakeBtn().setDisable(true);

                GameRulesView2 gameRulesView2 = new GameRulesView2();
                new GameRulesPresenter2(gameRulesView2,model);

                Stage stage = (Stage) view.getScene().getWindow();
                imageFitter(gameRulesView2.getBackgroundImage(),stage);


                view.getScene().setRoot(gameRulesView2);

            }
        });

        hoverAnimation(view.getTakeBtn());
    }

    private void updateView(){

    }
}
