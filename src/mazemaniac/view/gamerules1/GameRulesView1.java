package mazemaniac.view.gamerules1;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.util.Objects;

public class GameRulesView1 extends AnchorPane {

    private Button takeBtn;

    private ImageView backgroundImage;

    private VBox centerTakeVBox;



    public GameRulesView1() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes(){

        takeBtn = new Button("Take");

        Image image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/GameRules/RulesScreen.png")));
        backgroundImage = new ImageView(image);

        centerTakeVBox = new VBox(takeBtn);

        getChildren().addAll(backgroundImage,centerTakeVBox);
    }

    private void layoutNodes() {

        setLeftAnchor(takeBtn, 0.0);
        setRightAnchor(takeBtn, 0.0);
        setBottomAnchor(takeBtn, 0.0);

        takeBtn.setFont(Font.loadFont("file:resources/Font/IMFellDWPicaSC-Regular.ttf", 40));
        takeBtn.getStyleClass().add("generalButton");

        setLeftAnchor(centerTakeVBox, 0.0);
        setRightAnchor(centerTakeVBox, 0.0);
        setBottomAnchor(centerTakeVBox, 0.0);

        centerTakeVBox.setAlignment(Pos.CENTER);
        centerTakeVBox.setPadding(new Insets(0,0,10,0));


    }


    public Button getTakeBtn() {
        return takeBtn;
    }

    public ImageView getBackgroundImage() {
        return backgroundImage;
    }
}
