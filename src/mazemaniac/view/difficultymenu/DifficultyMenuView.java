package mazemaniac.view.difficultymenu;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.util.Objects;

public class DifficultyMenuView extends AnchorPane {
    private VBox VBox;
    private HBox HBox;


    private ImageView backgroundImage;

    private Button goRightBtn;
    private Button goLeftBtn;

    private Button applyBtn;
    private Button goToMainMenuBtn;

    private StackPane stackPane;
    private Label easyLbl;
    private Label intermediateLbl;
    private Label hardLbl;
    private Label savedLbl;











    public DifficultyMenuView() {
        initialiseNodes();
        layoutNodes();
    }







    private void initialiseNodes() {
        VBox = new VBox();
        HBox = new HBox();

        goRightBtn = new Button();
        goLeftBtn = new Button();

        stackPane = new StackPane();
        easyLbl = new Label("Easy");
        intermediateLbl = new Label("Intermediate");
        hardLbl = new Label("Hard");

        intermediateLbl.setOpacity(0);
        hardLbl.setOpacity(0);

        applyBtn = new Button("Apply");
        savedLbl = new Label("Saved!");

        goToMainMenuBtn = new Button("Back");

        Image image1 = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/DifficultyMenu/Wall.jpg")));
        backgroundImage = new ImageView(image1);

        Image image2 = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/DifficultyMenu/ArrowRight.png")));
        ImageView arrowRight = new ImageView(image2);
        goRightBtn.setGraphic(arrowRight);

        Image image3 = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/DifficultyMenu/ArrowLeft.png")));
        ImageView arrowLeft = new ImageView(image3);
        goLeftBtn.setGraphic(arrowLeft);

        stackPane.getChildren().addAll(easyLbl,intermediateLbl,hardLbl);
        HBox.getChildren().addAll(goLeftBtn,stackPane,goRightBtn);
        VBox.getChildren().addAll(HBox,applyBtn,savedLbl);
        getChildren().addAll(backgroundImage,VBox,goToMainMenuBtn);
    }

    public void layoutNodes() {

        stackPane.setPrefHeight(49);
        stackPane.setPrefWidth(236);

        easyLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),40));
        intermediateLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),40));
        hardLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),40));

        easyLbl.getStyleClass().add("labels");
        intermediateLbl.getStyleClass().add("labels");
        hardLbl.getStyleClass().add("labels");

        goRightBtn.getStyleClass().add("arrowButton");
        goLeftBtn.getStyleClass().add("arrowButton");

        applyBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),30));
        applyBtn.getStyleClass().add("generalButton");

        savedLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),20));
        savedLbl.getStyleClass().add("labels");
        savedLbl.setOpacity(0);

        goToMainMenuBtn.setLayoutX(20);
        goToMainMenuBtn.setLayoutY(14);
        goToMainMenuBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),15));

        HBox.setSpacing(20);
        HBox.setAlignment(Pos.CENTER);

        setLeftAnchor(HBox,0.0);
        setRightAnchor(HBox,0.0);
        setTopAnchor(HBox,0.0);
        setBottomAnchor(HBox,0.0);

        VBox.setSpacing(20);
        VBox.setAlignment(Pos.CENTER);
        VBox.setPrefHeight(200);
        VBox.setPrefWidth(551);

        setLeftAnchor(VBox,0.0);
        setRightAnchor(VBox,0.0);
        setTopAnchor(VBox,0.0);
        setBottomAnchor(VBox,0.0);

        setOpacity(0);

    }

    public HBox getHBox() {
        return HBox;
    }

    public Button getGoRightBtn() {
        return goRightBtn;
    }

    public Button getGoLeftBtn() {
        return goLeftBtn;
    }

    public Button getApplyBtn() {
        return applyBtn;
    }

    public Button getGoToMainMenuBtn() {
        return goToMainMenuBtn;
    }

    public StackPane getStackPane() {
        return stackPane;
    }

    public ImageView getBackgroundImage() {
        return backgroundImage;
    }

    public Label getSavedLbl() {
        return savedLbl;
    }


}
