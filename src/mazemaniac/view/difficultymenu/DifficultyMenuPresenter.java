package mazemaniac.view.difficultymenu;

import mazemaniac.model.mainmazecreation.GameManager;
import javafx.animation.ParallelTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import static mazemaniac.view.SceneUtils.*;


public class DifficultyMenuPresenter {
    private DifficultyMenuView view;
    private GameManager model;

    public DifficultyMenuPresenter(DifficultyMenuView view, GameManager model) {
        this.model = model;
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {

        view.getGoRightBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                int nextIndex = 0;
                for (int i = 0; i < view.getStackPane().getChildren().size(); i++) {

                    nextIndex = (i + 1) % view.getStackPane().getChildren().size(); //will always get the next index (i=4 --> i = 0)

                    if (view.getStackPane().getChildren().get(i).getOpacity() != 0) {
                        int finalI = i; //Because inner class won't except outside variable that isn't final (i changes value).

                        ParallelTransition parallelTransition = new ParallelTransition();
                        parallelTransition.getChildren().addAll(moveToAnimation(view.getStackPane().getChildren().get(i), 50, 'x'),
                                fadeAnimation(view.getStackPane().getChildren().get(i), 0, 0.3));

                        parallelTransition.play();
                        parallelTransition.setOnFinished(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                view.getStackPane().getChildren().get(finalI).setTranslateX(0); // Reset the translation to its original position
                            }
                        });
                        break;
                    }

                }
                fadeAnimation(view.getStackPane().getChildren().get(nextIndex), 1, 0.3).play();
            }
        });
        hoverAnimation(view.getGoRightBtn());
        view.getGoRightBtn().setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                onHoverOnReleaseAnimation(view.getGoRightBtn(),1.3,0.9);
            }
        });

        view.getGoLeftBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                int prevIndex = 0;
                for (int i = view.getStackPane().getChildren().size() - 1; i >= 0; i--) {

                    prevIndex = (i - 1) % view.getStackPane().getChildren().size(); //same as goRightBtn
                    if (prevIndex == -1) { //formula falls apart at i = 0. so manually fix it.
                        prevIndex = view.getStackPane().getChildren().size() - 1;
                    }
                    if (view.getStackPane().getChildren().get(i).getOpacity() != 0) { //all but one child will have opacity = 1
                        int finalI = i;

                        ParallelTransition parallelTransition = new ParallelTransition();
                        parallelTransition.getChildren().addAll(moveToAnimation(view.getStackPane().getChildren().get(i), -50, 'x'),
                                fadeAnimation(view.getStackPane().getChildren().get(i), 0, 0.3));

                        parallelTransition.play();
                        parallelTransition.setOnFinished(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                view.getStackPane().getChildren().get(finalI).setTranslateX(0); // Reset the translation to its original position
                            }
                        });
                        break;
                    }
                }
                fadeAnimation(view.getStackPane().getChildren().get(prevIndex), 1, 0.3).play();
            }
        });
        hoverAnimation(view.getGoLeftBtn());
        view.getGoLeftBtn().setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                onHoverOnReleaseAnimation(view.getGoLeftBtn(),1.3,0.9);
            }
        });

        view.getApplyBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String difficulty = "easy"; //default = easy
                for (Node node : view.getStackPane().getChildren()) {
                    Label label = (Label) node;
                    if (label.getOpacity() != 0) { //all but one child will have opacity = 1
                        difficulty = label.getText();
                        break;
                    }
                }

                view.getSavedLbl().setOpacity(1);
                fadeAnimation(view.getSavedLbl(), 0, 1).play();
                model.setDifficultySelector(difficulty);
            }
        });
        hoverAnimation(view.getApplyBtn());



        view.getGoToMainMenuBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                disableAllButtons();

                ParallelTransition parallelTransition = new ParallelTransition(fadeAnimation(view.getScene().getRoot(), 0, 0.3),
                        moveToAnimation(view.getHBox(), +100, 'y'),
                        moveToAnimation(view.getApplyBtn(), +50, 'y'));
                parallelTransition.play();
                parallelTransition.setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        toMainMenu(view.getGoToMainMenuBtn(),model);
                    }
                });


            }
        });
        hoverAnimation(view.getGoToMainMenuBtn());
    }

    private void updateView() {


    }

    private void disableAllButtons(){
        view.getGoToMainMenuBtn().setDisable(true);
        view.getApplyBtn().setDisable(true);
        view.getGoRightBtn().setDisable(true);
        view.getGoLeftBtn().setDisable(true);
    }







}
