package mazemaniac.view.loadingscreen;

import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.mainmenu.MainMenuPresenter;
import mazemaniac.view.mainmenu.MainMenuView;
import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.util.Duration;

import static mazemaniac.view.SceneUtils.fadeAnimation;
import static mazemaniac.view.SceneUtils.imageFitter;

public class LoadingScreenPresenter {
    private LoadingScreenView view;

    private GameManager model;

    private Timeline timeline;

    public LoadingScreenPresenter(LoadingScreenView view, GameManager model) {
        this.view = view;
        this.model = model;

        this.timeline = updateView();
        timeline.play();
        addEventHandlers();

    }

    private void addEventHandlers() {
        timeline.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                MainMenuView mainMenuView = new MainMenuView();
                new MainMenuPresenter(mainMenuView, model);

                fadeAnimation(view.getScene().getRoot(),0,0.5);

                view.getScene().setRoot(mainMenuView);


                Stage stage = (Stage) mainMenuView.getScene().getWindow();
                imageFitter(mainMenuView.getSplashArtImage(),stage);


                fadeAnimation(mainMenuView.getScene().getRoot(), 0.99, 0.3).play();
            }
        });
    }

    private Timeline updateView() {
        Timeline timeline = new Timeline();
        KeyFrame firstFrame = new KeyFrame(Duration.seconds(0), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                view.getStackPane().getChildren().get(0).setOpacity(1);
            }
        });

        KeyFrame secondFrame = new KeyFrame(Duration.seconds(0.7), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                view.getStackPane().getChildren().get(0).setOpacity(0);
                view.getStackPane().getChildren().get(1).setOpacity(1);
            }
        });

        KeyFrame thirdFrame = new KeyFrame(Duration.seconds(1.4), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                view.getStackPane().getChildren().get(1).setOpacity(0);
                view.getStackPane().getChildren().get(2).setOpacity(1);
            }
        });

        KeyFrame fourthFrame = new KeyFrame(Duration.seconds(2.1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                view.getStackPane().getChildren().get(2).setOpacity(0);
                view.getStackPane().getChildren().get(3).setOpacity(1);
            }
        });

        KeyFrame fifthFrame = new KeyFrame(Duration.seconds(2.8), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                view.getStackPane().getChildren().get(3).setOpacity(0);
            }
        });

        //after X duration go to the next frame
        timeline.getKeyFrames().addAll(firstFrame, secondFrame, thirdFrame, fourthFrame,fifthFrame);
        timeline.setCycleCount(1);
        return  timeline;
    }








}
