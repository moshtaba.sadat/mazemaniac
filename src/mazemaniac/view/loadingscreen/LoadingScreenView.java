package mazemaniac.view.loadingscreen;


import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.Objects;

public class LoadingScreenView extends AnchorPane {

    private Label loadingZeroDotLbl;

    private Label loadingFirstDotLbl;

    private  Label loadingSecondDotLbl;

    private Label loadingThirdDotLbl;

    private StackPane stackPane;

    private ImageView splashArtImage;


    public LoadingScreenView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {

        loadingZeroDotLbl = new Label("Loading");
        loadingFirstDotLbl = new Label("Loading.");
        loadingSecondDotLbl = new Label("Loading..");
        loadingThirdDotLbl = new Label("Loading...");

        stackPane = new StackPane();

        Image image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/LoadingScreen/SplashArt.png")));
        splashArtImage = new ImageView(image);


    }

    public void layoutNodes() {

        getChildren().addAll(splashArtImage,stackPane);
        stackPane.getChildren().addAll(loadingZeroDotLbl,loadingFirstDotLbl,loadingSecondDotLbl,loadingThirdDotLbl);

        setBottomAnchor(stackPane,0.0);
        setLeftAnchor(stackPane,0.0);

        loadingZeroDotLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 80));
        loadingFirstDotLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 80));
        loadingSecondDotLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 80));
        loadingThirdDotLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 80));

        loadingZeroDotLbl.setTextFill(Color.BLACK);
        loadingFirstDotLbl.setTextFill(Color.BLACK);
        loadingSecondDotLbl.setTextFill(Color.BLACK);
        loadingThirdDotLbl.setTextFill(Color.BLACK);


        loadingFirstDotLbl.setOpacity(0);
        loadingSecondDotLbl.setOpacity(0);
        loadingThirdDotLbl.setOpacity(0);

    }

    public StackPane getStackPane() {
        return stackPane;
    }

    public ImageView getSplashArtImage() {
        return splashArtImage;
    }
}
