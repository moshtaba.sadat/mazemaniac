package mazemaniac.view.mainmenu;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.Objects;

public class MainMenuView extends StackPane {
    private Button PlayBtn;
    private Button DifficultyBtn;
    private Button ExitBtn;

    private Button leaderBordBtn;
    private Label titleLbl;

    private VBox vBox;

    private ImageView splashArtImage;

    public ImageView getSplashArtImage() {
        return splashArtImage;
    }

    public MainMenuView(){

        this.initialiseNodes();
        this.layoutNodes();

    }

    private void initialiseNodes() {
        titleLbl = new Label("Maze Maniac");
        PlayBtn = new Button("Play");
        DifficultyBtn = new Button("Difficulty");
        ExitBtn = new Button("Exit");
        leaderBordBtn = new Button("LeaderBord");
        vBox = new VBox(titleLbl,PlayBtn,DifficultyBtn,leaderBordBtn,ExitBtn);

        Image image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/MainMenu/MainMenuSplashArt.png")));
        splashArtImage = new ImageView(image);

        getChildren().addAll(splashArtImage,vBox);
    }

    private void layoutNodes() {
        setAlignment(Pos.CENTER);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(30);

        titleLbl.setPadding(new Insets(0, 0, 20, 0));

        PlayBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),15));
        DifficultyBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 15));
        ExitBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 15));
        leaderBordBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 15));

        PlayBtn.getStyleClass().add("buttonsMainMenu");
        DifficultyBtn.getStyleClass().add("buttonsMainMenu");
        ExitBtn.getStyleClass().add("buttonsMainMenu");
        leaderBordBtn.getStyleClass().add("buttonsMainMenu");

        titleLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 60));
        titleLbl.setTextFill(Color.BLACK);

        PlayBtn.setPrefSize(140, 0);
        DifficultyBtn.setPrefSize(140, 0);
        ExitBtn.setPrefSize(140, 0);
        leaderBordBtn.setPrefSize(140, 0);

        setOpacity(0);

    }

    public Button getPlayBtn() {
        return PlayBtn;
    }

    public Button getDifficultyBtn() {
        return DifficultyBtn;
    }

    public Button getExitBtn() {
        return ExitBtn;
    }

    public Label getTitleLbl() {
        return titleLbl;
    }

    public Button getLeaderBordBtn() {
        return leaderBordBtn;
    }
}
