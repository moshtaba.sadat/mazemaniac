package mazemaniac.view.mainmenu;


import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.difficultymenu.DifficultyMenuPresenter;
import mazemaniac.view.difficultymenu.DifficultyMenuView;
import mazemaniac.view.leaderboardbracketselection.LeaderBoardBracketSelectionPresenter;
import mazemaniac.view.leaderboardbracketselection.LeaderBoardBracketSelectionView;
import mazemaniac.view.nameselector.NameSelectorPresenter;
import mazemaniac.view.nameselector.NameSelectorView;
import javafx.animation.ParallelTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;


import static mazemaniac.view.SceneUtils.*;


public class MainMenuPresenter  {
    private MainMenuView view;
    private GameManager model;



    public MainMenuPresenter(MainMenuView view, GameManager model) {
        this.model = model;
        this.view = view;
        this.addEventHandlers();
        this.updateView();
    }

    private void addEventHandlers(){

        view.getDifficultyBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                disableAllButtons();

                DifficultyMenuView difficultyMenuView = new DifficultyMenuView();
                new DifficultyMenuPresenter(difficultyMenuView, model);

                animation().setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {

                        view.getScene().setRoot(difficultyMenuView);
                        Stage stage = (Stage) difficultyMenuView.getScene().getWindow();
                        imageFitter(difficultyMenuView.getBackgroundImage(),stage);
                        fadeAnimation(difficultyMenuView.getScene().getRoot(),1,0.33).play();
                    }
                });
            }
        });
        hoverAnimation(view.getDifficultyBtn());

        view.getPlayBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                disableAllButtons();

                NameSelectorView nameSelectorView = new NameSelectorView();
                new NameSelectorPresenter(nameSelectorView,model);

                animation().setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        view.getScene().setRoot(nameSelectorView);

                        Stage stage = (Stage) nameSelectorView.getScene().getWindow();
                        imageFitter(nameSelectorView.getBackgroundImage(),stage);

                        fadeAnimation(nameSelectorView.getScene().getRoot(),1,0.3).play();
                    }
                });
            }
        });
        hoverAnimation(view.getPlayBtn());

        view.getLeaderBordBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                disableAllButtons();
                LeaderBoardBracketSelectionView leaderBoardBracketSelectionView = new LeaderBoardBracketSelectionView();
                new LeaderBoardBracketSelectionPresenter(leaderBoardBracketSelectionView,model);

                animation().setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {

                        view.getScene().setRoot(leaderBoardBracketSelectionView);

                        Stage stage = (Stage) leaderBoardBracketSelectionView.getScene().getWindow();
                        imageFitter(leaderBoardBracketSelectionView.getBackgroundImage(),stage);

                        fadeAnimation(leaderBoardBracketSelectionView,1,0.3).play();

                    }
                });
            }
        });
        hoverAnimation(view.getLeaderBordBtn());


        view.getExitBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Stage stage = (Stage) view.getScene().getWindow();
                exit(stage,event,"Leaving already?","Stay and play another game");
            }
        });
        hoverAnimation(view.getExitBtn());


    }

    private void updateView(){

    }

    private ParallelTransition animation(){

         ParallelTransition parallelTransition = new ParallelTransition(moveToAnimation(view.getScene().getRoot(), +50,'y'),
                                                                        moveToAnimation(view.getTitleLbl(), -100, 'y'),
                                                                        fadeAnimation(view.getScene().getRoot(), 0,0.3));
         parallelTransition.play();

        return parallelTransition;

    }

    private void disableAllButtons(){
        view.getExitBtn().setDisable(true);
        view.getLeaderBordBtn().setDisable(true);
        view.getPlayBtn().setDisable(true);
        view.getDifficultyBtn().setDisable(true);
    }





}
