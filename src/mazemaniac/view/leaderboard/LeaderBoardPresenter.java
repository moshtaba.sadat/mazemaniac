package mazemaniac.view.leaderboard;

import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.model.mainmazecreation.PlayerScore;
import mazemaniac.view.leaderboardbracketselection.LeaderBoardBracketSelectionPresenter;
import mazemaniac.view.leaderboardbracketselection.LeaderBoardBracketSelectionView;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import static mazemaniac.view.SceneUtils.*;

public class LeaderBoardPresenter {
    LeaderBoardView view;

    GameManager model;

    public LeaderBoardPresenter(LeaderBoardView view, GameManager model){
        this.view = view;
        this.model = model;

        try {
            this.model.getFileManager().readFromFile();
            this.model.getFileManager().sort();


            addEventHandlers();
            updateView();


            this.model.getFileManager().save();
            this.model.getFileManager().getPlayerScores().clear();
            this.model.setDifficultySelector("Easy");
        }
        catch (RuntimeException e){
            catchFileException();
        }
    }

    private void addEventHandlers()  {
        view.getBackToLeaderBoardSelectionBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                view.getBackToLeaderBoardSelectionBtn().setDisable(true);



                LeaderBoardBracketSelectionView leaderBoardBracketSelectionView =
                        new LeaderBoardBracketSelectionView();
                new LeaderBoardBracketSelectionPresenter(leaderBoardBracketSelectionView,model);

                FadeTransition fadeTransition = fadeAnimation(view.getScene().getRoot(),0,0.3);
                fadeTransition.play();
                fadeTransition.setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                            view.getScene().setRoot(leaderBoardBracketSelectionView);
                            Stage stage = (Stage) leaderBoardBracketSelectionView.getScene().getWindow();
                            imageFitter(leaderBoardBracketSelectionView.getBackgroundImage(),stage);
                            fadeAnimation(leaderBoardBracketSelectionView,1,0.3).play();

                    }
                });
            }
        });

        hoverAnimation(view.getBackToLeaderBoardSelectionBtn());
    }

    private void updateView(){

        PlayerScore aPlayerScore;

        for (int i = 0; i < model.getFileManager().getPlayerScores().size(); ++i){
            aPlayerScore = model.getFileManager().getPlayerScores().get(i);
            GridPane gridPane = recordMaker(aPlayerScore.getName(),aPlayerScore.getPoints(),
                    aPlayerScore.getMazesSolved(),aPlayerScore.getCoinsCollected(),i+1);
            view.getvBox().getChildren().add(gridPane);
        }

    }

    private GridPane recordMaker(String name, int score, int mazeCompletions, int coinscollected,int i){


        Label nameLbl = new Label(String.format("%d.   %s", i,name));
        Label scoreLbl = new Label(String.format("%d", score));
        Label mazeCompletionLbl = new Label(String.format("%d",mazeCompletions));
        Label coinsCollectedLbl = new Label(String.format("%d",coinscollected));


        GridPane gridPane = new GridPane();

        gridPane.add(nameLbl,0,0);
        gridPane.add(scoreLbl, 1,0);
        gridPane.add(mazeCompletionLbl,2,0);
        gridPane.add(coinsCollectedLbl,3,0);

        ColumnConstraints columnConstraintsName = new ColumnConstraints(250);
        ColumnConstraints columnConstraintsScore = new ColumnConstraints(200);
        GridPane.setHalignment(scoreLbl, HPos.RIGHT);
        ColumnConstraints columnConstraintsMazes = new ColumnConstraints(200);
        GridPane.setHalignment(mazeCompletionLbl, HPos.RIGHT);
        ColumnConstraints columnConstraintsCoins = new ColumnConstraints(200);
        GridPane.setHalignment(coinsCollectedLbl, HPos.RIGHT);


        nameLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 25));
        scoreLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 25));
        mazeCompletionLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 25));
        coinsCollectedLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 25));

        nameLbl.getStyleClass().add("labels");
        scoreLbl.getStyleClass().add("labels");
        mazeCompletionLbl.getStyleClass().add("labels");
        coinsCollectedLbl.getStyleClass().add("labels");


        gridPane.getColumnConstraints().addAll(columnConstraintsName,columnConstraintsScore,columnConstraintsMazes,columnConstraintsCoins);
        gridPane.setAlignment(Pos.CENTER);


        return gridPane;



    }

    private void catchFileException(){

        view.getBackToLeaderBoardSelectionBtn().setDisable(true);
        FadeTransition fadeTransition1 = fadeAnimation(view.getFileNotFoundLabel(), 1 , 2.5);
        fadeTransition1.play();
        fadeTransition1.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                toMainMenu(view.getBackToLeaderBoardSelectionBtn(),model);

            }
        });
    }






}
