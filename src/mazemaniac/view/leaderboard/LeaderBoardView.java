package mazemaniac.view.leaderboard;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.Objects;

public class LeaderBoardView extends AnchorPane {

    private Label fileNotFoundLabel;

    private GridPane gridPane;

    private Label nameLbl;

    private Label scoreLbl;

    private Label mazesSolvedLbl;

    private Label coinsCollectedLbl;

    private VBox vBox;

    private Button backToLeaderBoardSelectionBtn;

    private ImageView backgroundImage;


    public LeaderBoardView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes(){
        fileNotFoundLabel = new Label("Can't find file, returning to Main Menu");

        backToLeaderBoardSelectionBtn = new Button("Back");

        nameLbl = new Label("Name");
        scoreLbl = new Label("Score");
        mazesSolvedLbl = new Label("Mazes Solved");
        coinsCollectedLbl = new Label("Coins Collected");

        gridPane = new GridPane();

        Image image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/LeaderBord/LeaderBord.png")));
        backgroundImage = new ImageView(image);

        gridPane.add(nameLbl,0,0);
        gridPane.add(scoreLbl, 1,0);
        gridPane.add(mazesSolvedLbl,2,0);
        gridPane.add(coinsCollectedLbl,3,0);

        ColumnConstraints columnConstraintsName = new ColumnConstraints(250);

        ColumnConstraints columnConstraintsScore = new ColumnConstraints(200);
        GridPane.setHalignment(scoreLbl, HPos.RIGHT);
        ColumnConstraints columnConstraintsMazes = new ColumnConstraints(200);
        GridPane.setHalignment(mazesSolvedLbl, HPos.RIGHT);
        ColumnConstraints columnConstraintsCoins = new ColumnConstraints(200);
        GridPane.setHalignment(coinsCollectedLbl, HPos.RIGHT);


        gridPane.getColumnConstraints().addAll(columnConstraintsName,columnConstraintsScore,columnConstraintsMazes,columnConstraintsCoins);

        vBox = new VBox(gridPane);
        getChildren().addAll(backgroundImage,fileNotFoundLabel,vBox, backToLeaderBoardSelectionBtn);
    }

    private void layoutNodes(){
        setOpacity(0);

        fileNotFoundLabel.setFont(new Font(50));
        fileNotFoundLabel.setOpacity(0);
        fileNotFoundLabel.setTextFill(Color.RED);
        setLeftAnchor(fileNotFoundLabel,0.0);
        setRightAnchor(fileNotFoundLabel,0.0);
        setTopAnchor(fileNotFoundLabel,0.0);
        setBottomAnchor(fileNotFoundLabel,0.0);


        nameLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 25));
        scoreLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 25));
        mazesSolvedLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 25));
        coinsCollectedLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 25));

        nameLbl.getStyleClass().add("labels");
        scoreLbl.getStyleClass().add("labels");
        mazesSolvedLbl.getStyleClass().add("labels");
        coinsCollectedLbl.getStyleClass().add("labels");

        backToLeaderBoardSelectionBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 15));

        gridPane.setAlignment(Pos.CENTER);

        vBox.setSpacing(0);
        vBox.setAlignment(Pos.TOP_CENTER);
        vBox.setPadding(new Insets(20,0,20,0));

        setRightAnchor(vBox, 0.0);
        setLeftAnchor(vBox, 0.0);
        setBottomAnchor(vBox, 0.0);
        setTopAnchor(vBox, 0.0);


        backToLeaderBoardSelectionBtn.setLayoutX(20);
        backToLeaderBoardSelectionBtn.setLayoutY(14);


    }

    public VBox getvBox() {
        return vBox;
    }

    public Button getBackToLeaderBoardSelectionBtn() {
        return backToLeaderBoardSelectionBtn;
    }


    public ImageView getBackgroundImage() {
        return backgroundImage;
    }

    public Label getFileNotFoundLabel() {
        return fileNotFoundLabel;
    }
}
