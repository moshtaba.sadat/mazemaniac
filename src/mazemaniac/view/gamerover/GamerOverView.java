package mazemaniac.view.gamerover;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.Objects;

public class GamerOverView extends AnchorPane {

    private ImageView imageViewBackground;

    private Label gameOverLbl;

    private HBox scoreSummaryHBox;

    private HBox buttonsHBox;


    private Label nameLbl;

    private Label  difficultyLbl;

    private Label scoreLbl;

    private Label mazesSolvedLbl;

    private Label coinsCollectedLbl;

    private Button continueBtn;

    private Button exitBtn;

    private VBox allNodeVBox;

    public GamerOverView(){
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes(){
        gameOverLbl = new Label("GAME OVER");
        continueBtn = new Button("Continue");
        exitBtn = new Button("Exit");

        nameLbl = new Label();
        difficultyLbl = new Label();
        scoreLbl = new Label();
        mazesSolvedLbl = new Label();
        coinsCollectedLbl = new Label();

        scoreSummaryHBox= new HBox(nameLbl,difficultyLbl,scoreLbl,mazesSolvedLbl,coinsCollectedLbl);
        buttonsHBox = new HBox(exitBtn,continueBtn);

        allNodeVBox = new VBox(scoreSummaryHBox,buttonsHBox);

        Image image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/GameOver/GameOverScreen(&).png")));
        imageViewBackground = new ImageView(image);

        getChildren().addAll(imageViewBackground,allNodeVBox);

    }

    private void layoutNodes(){
        allNodeVBox.setAlignment(Pos.CENTER);
        allNodeVBox.setSpacing(30);

        nameLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 20));
        difficultyLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 20));
        scoreLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 20));
        mazesSolvedLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 20));
        coinsCollectedLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 20));

        nameLbl.setTextFill(Color.WHITE);
        difficultyLbl.setTextFill(Color.WHITE);
        scoreLbl.setTextFill(Color.WHITE);
        mazesSolvedLbl.setTextFill(Color.WHITE);
        coinsCollectedLbl.setTextFill(Color.WHITE);

        exitBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),30));
        continueBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 30));

        exitBtn.getStyleClass().add("generalButton");
        continueBtn.getStyleClass().add("generalButton");

        exitBtn.setMinSize(80,0);
        continueBtn.setMinSize(80,0);

        scoreSummaryHBox.setSpacing(10);
        scoreSummaryHBox.setAlignment(Pos.CENTER);

        buttonsHBox.setSpacing(10);
        buttonsHBox.setAlignment(Pos.CENTER);




        setBottomAnchor(allNodeVBox,40.0);
        setLeftAnchor(allNodeVBox, 170.0);



    }

    public Label getGameOverLbl() {
        return gameOverLbl;
    }

    public Button getExitBtn() {
        return exitBtn;
    }

    public ImageView getImageViewBackground() {
        return imageViewBackground;
    }

    public Label getNameLbl() {
        return nameLbl;
    }

    public Label getDifficultyLbl() {
        return difficultyLbl;
    }

    public Label getScoreLbl() {
        return scoreLbl;
    }

    public Label getMazesSolvedLbl() {
        return mazesSolvedLbl;
    }

    public Label getCoinsCollectedLbl() {
        return coinsCollectedLbl;
    }

    public Button getContinueBtn() {
        return continueBtn;
    }
}
