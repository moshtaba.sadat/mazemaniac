package mazemaniac.view.gamerover;

import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.loadingscreen.LoadingScreenPresenter;
import mazemaniac.view.loadingscreen.LoadingScreenView;
import javafx.animation.ParallelTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;


import static mazemaniac.view.SceneUtils.*;

public class GamerOverPresenter {

    private GamerOverView view;

    private GameManager model;

    public GamerOverPresenter(GamerOverView view, GameManager model) {
        this.view = view;
        this.model = model;

        updateView();
        addEventHandlers();
    }

    private void addEventHandlers(){
        view.getContinueBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                disableAllButtons();

                LoadingScreenView loadingScreenView = new LoadingScreenView();
                model = new GameManager();
                new LoadingScreenPresenter(loadingScreenView,model);

                ParallelTransition parallelTransition =
                        new ParallelTransition(fadeAnimation(view.getScene().getRoot(),0,0.3),
                                                moveToAnimation(view.getGameOverLbl(),50,'y'),
                                                moveToAnimation(view.getContinueBtn(),-50,'y'));

                parallelTransition.play();
                parallelTransition.setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        view.getScene().setRoot(loadingScreenView);
                        Stage stage = (Stage) loadingScreenView.getScene().getWindow();
                        imageFitter(loadingScreenView.getSplashArtImage(),stage);
                    }
                });

            }
        });

        hoverAnimation(view.getContinueBtn());

        view.getExitBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                Stage stage = (Stage) view.getScene().getWindow();
                exit(stage,event,"You scored so bad that you are rage quiting?","Yikes");
            }
        });
        hoverAnimation(view.getExitBtn());

    }



    private void updateView(){
        view.getNameLbl().setText("Name: " + model.getPlayer().getPlayerName());
        view.getDifficultyLbl().setText("Difficulty: " + model.getDifficultySelector());
        view.getScoreLbl().setText("Score: " + model.getPlayer().getPoints());
        view.getMazesSolvedLbl().setText("Mazes solved: " + model.getPlayer().getAmountOfMazeCompletions());
        view.getCoinsCollectedLbl().setText("Coins collected: " + model.getPlayer().getCoinAmount());
    }

    private void disableAllButtons(){
        view.getContinueBtn().setDisable(true);
        view.getExitBtn().setDisable(true);
    }
}
