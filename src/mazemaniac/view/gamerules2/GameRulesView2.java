package mazemaniac.view.gamerules2;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.util.Objects;

public class GameRulesView2 extends AnchorPane {

    private ImageView backgroundImage;

    private Button enterDungeonBtn;

    private VBox centerRulesVBox;

    public GameRulesView2() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {

        Image image1 = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/GameRules/RulesScreen_2.png")));
        backgroundImage = new ImageView(image1);

        Image image2 = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/GameRules/Rules (55)_transparent.png")));
        ImageView rulesImage = new ImageView(image2);

        enterDungeonBtn = new Button("Enter the dungeon");

        centerRulesVBox = new VBox(rulesImage, enterDungeonBtn);

        getChildren().addAll(backgroundImage, centerRulesVBox);
    }

    private void layoutNodes() {

        setLeftAnchor(centerRulesVBox, 0.0);
        setRightAnchor(centerRulesVBox, 0.0);
        setBottomAnchor(centerRulesVBox, 0.0);
        setTopAnchor(centerRulesVBox, 0.0);

        enterDungeonBtn.getStyleClass().add("generalButton");
        enterDungeonBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),40));

        centerRulesVBox.setAlignment(Pos.CENTER);
        centerRulesVBox.setSpacing(12);


    }

    public Button getEnterDungeonBtn() {
        return enterDungeonBtn;
    }

    public ImageView getBackgroundImage() {
        return backgroundImage;
    }

}
