package mazemaniac.view.gamerules2;

import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.buffer.BufferPresenter;
import mazemaniac.view.buffer.BufferView;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

import static mazemaniac.view.SceneUtils.*;

public class GameRulesPresenter2 {

    private GameRulesView2 view;

    private GameManager model;

    public GameRulesPresenter2(GameRulesView2 view, GameManager model) {
        this.view = view;
        this.model = model;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers(){
        view.getEnterDungeonBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                view.getEnterDungeonBtn().setDisable(true);

                BufferView bufferView = new BufferView();
                new BufferPresenter(bufferView,model);


               FadeTransition fadeTransition =  fadeAnimation(view.getScene().getRoot(),0,0.3);
               fadeTransition.play();
               fadeTransition.setOnFinished(new EventHandler<ActionEvent>() {
                   @Override
                   public void handle(ActionEvent event) {
                       view.getScene().setRoot(bufferView);

                       Stage stage = (Stage) bufferView.getScene().getWindow();
                       imageFitter(bufferView.getBackgroundImage(),stage);

                       if(model.getDifficultySelector().equalsIgnoreCase("hard")){
                           stage.setFullScreen(true);
                       }


                   }
               });
            }
        });

        hoverAnimation(view.getEnterDungeonBtn());
    }

    private void updateView(){

    }
}
