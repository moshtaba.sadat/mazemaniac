package mazemaniac.view.leaderboardbracketselection;

import javafx.animation.FadeTransition;
import javafx.scene.control.Button;
import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.leaderboard.LeaderBoardPresenter;
import mazemaniac.view.leaderboard.LeaderBoardView;
import javafx.animation.ParallelTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import static mazemaniac.view.SceneUtils.*;



public class LeaderBoardBracketSelectionPresenter {

    LeaderBoardBracketSelectionView view;

    GameManager model;

    public LeaderBoardBracketSelectionPresenter(LeaderBoardBracketSelectionView view, GameManager model) {
        this.model = model;
        this.view = view;
        this.addEventHandlers();
        this.updateView();
    }

    private void addEventHandlers(){

        hoverAnimation(view.getClearLeaderBordBtn());

        view.getEasyItemBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                clearLeaderBoard("easy");
            }
        });
        hoverAnimation(view.getEasyItemBtn());

        view.getIntermediateItemBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                clearLeaderBoard("intermediate");
            }
        });
        hoverAnimation(view.getIntermediateItemBtn());

        view.getHardItemBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                clearLeaderBoard("hard");
            }
        });
        hoverAnimation(view.getHardItemBtn());



        view.getEasyLeaderBoardBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                sceneHandler(view.getEasyLeaderBoardBtn());
            }
        });
        hoverAnimation(view.getEasyLeaderBoardBtn());

        view.getIntermediateLeaderBoardBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                sceneHandler(view.getIntermediateLeaderBoardBtn());
            }
        });
        hoverAnimation(view.getIntermediateLeaderBoardBtn());

        view.getHardLeaderBoardBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    sceneHandler(view.getHardLeaderBoardBtn());
            }
        });
        hoverAnimation(view.getHardLeaderBoardBtn());

        view.getGoToMainMenuBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                disableAllButtons();

                ParallelTransition parallelTransition = new ParallelTransition(
                        fadeAnimation(view.getScene().getRoot(), 0, 0.3));
                parallelTransition.play();

                parallelTransition.setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        toMainMenu(view.getGoToMainMenuBtn(),model);
                    }
                });

            }
        });
        hoverAnimation(view.getGoToMainMenuBtn());

    }

    private void updateView(){

    }

    private void sceneHandler(Button button){
        model.getFileManager().setDifficulty(button.getText());
        disableAllButtons();
        sceneSwitcher();
    }

    private void sceneSwitcher(){
        LeaderBoardView leaderBoardView = new LeaderBoardView();
        new LeaderBoardPresenter(leaderBoardView,model);

        animation().setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                view.getScene().setRoot(leaderBoardView);


                Stage stage = (Stage) leaderBoardView.getScene().getWindow();
                imageFitter(leaderBoardView.getBackgroundImage(),stage);

                fadeAnimation(leaderBoardView.getScene().getRoot(),1,0.33).play();
            }
        });
    }

    private ParallelTransition animation(){
        ParallelTransition parallelTransition = new ParallelTransition(
                fadeAnimation(view.getScene().getRoot(),0,0.3),
                moveToAnimation(view.getEasyLeaderBoardBtn(),50,'y'),
                moveToAnimation(view.getHardLeaderBoardBtn(),-50,'y'));
        parallelTransition.play();
        return parallelTransition;
    }

    private void disableAllButtons(){
        view.getEasyLeaderBoardBtn().setDisable(true);
        view.getIntermediateLeaderBoardBtn().setDisable(true);
        view.getHardLeaderBoardBtn().setDisable(true);
        view.getGoToMainMenuBtn().setDisable(true);
        view.getClearLeaderBordBtn().setDisable(true);
        view.getIntermediateItemBtn().setDisable(true);
        view.getEasyItemBtn().setDisable(true);
        view.getHardItemBtn().setDisable(true);

    }

    private void clearLeaderBoard(String difficulty){
        try{
            model.getFileManager().setDifficulty(difficulty);
            model.getFileManager().clearLeaderBoard();
        }
        catch (Exception e){
            fileNotfound();
        }

    }

    private void fileNotfound(){
        view.getFileNotFoundLabel().setOpacity(1);

        FadeTransition fadeTransition;
        fadeTransition = fadeAnimation(view.getFileNotFoundLabel(),0,2);
        fadeTransition.play();
        fadeTransition.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                toMainMenu(view.getGoToMainMenuBtn(),model);
            }
        });
    }
}
