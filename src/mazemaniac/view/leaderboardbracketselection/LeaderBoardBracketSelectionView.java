package mazemaniac.view.leaderboardbracketselection;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.Objects;

public class LeaderBoardBracketSelectionView extends AnchorPane {

    private Label fileNotFoundLabel;
    private MenuBar backAndClearBtn;


    private Button goToMainMenuBtn;

    private Button clearLeaderBordBtn;

    private Button easyItemBtn;

    private Button intermediateItemBtn;

    private Button hardItemBtn;


    private Label chooseLbl;

    private Button easyLeaderBoardBtn;

    private Button intermediateLeaderBoardBtn;

    private Button hardLeaderBoardBtn;

    private VBox vBox;

    private ImageView backgroundImage;

    public LeaderBoardBracketSelectionView(){
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes(){

        fileNotFoundLabel = new Label("File not found, returning to Main Menu");


        goToMainMenuBtn = new Button("Back");
        Menu backMenu = new Menu(null, goToMainMenuBtn);

        easyItemBtn = new Button("Clear Easy");
        MenuItem easyItem = new MenuItem(null, easyItemBtn);

        intermediateItemBtn = new Button("Clear Intermediate");
        MenuItem intermediateItem = new MenuItem(null, intermediateItemBtn);

        hardItemBtn = new Button("Clear Hard");
        MenuItem hardItem = new MenuItem(null, hardItemBtn);


        clearLeaderBordBtn = new Button("Reset LeaderBoard");
        Menu clearLeaderBordMenu = new Menu(null, clearLeaderBordBtn, easyItem, intermediateItem, hardItem);


        backAndClearBtn = new MenuBar(backMenu, clearLeaderBordMenu);

        chooseLbl = new Label("Choose the leaderbord that you want to look at");
        easyLeaderBoardBtn = new Button("Easy");
        intermediateLeaderBoardBtn = new Button("Intermediate");
        hardLeaderBoardBtn = new Button("Hard");

        Image image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/LeaderBord/LeaderBordSelector.png")));
        backgroundImage = new ImageView(image);


        vBox = new VBox(chooseLbl, easyLeaderBoardBtn, intermediateLeaderBoardBtn, hardLeaderBoardBtn);


        getChildren().addAll(backgroundImage,fileNotFoundLabel,vBox,backAndClearBtn);
    }

    private void layoutNodes(){
        fileNotFoundLabel.setFont(new Font(50));
        setLeftAnchor(fileNotFoundLabel,70.0);
        setRightAnchor(fileNotFoundLabel,0.0);
        setTopAnchor(fileNotFoundLabel,0.0);
        fileNotFoundLabel.setOpacity(0);
        fileNotFoundLabel.setTextFill(Color.RED);



        buttonStyler(goToMainMenuBtn);
        buttonStyler(clearLeaderBordBtn);
        buttonStyler(easyItemBtn);
        buttonStyler(intermediateItemBtn);
        buttonStyler(hardItemBtn);

        backAndClearBtn.getStyleClass().add("menu-item");



        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(30);

        chooseLbl.setPadding(new Insets(0, 0, 20, 0));

        chooseLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),25));

        easyLeaderBoardBtn.setMinSize(80, 0);
        intermediateLeaderBoardBtn.setMinSize(80,0);
        hardLeaderBoardBtn.setMinSize(80,0);

        easyLeaderBoardBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),25));
        intermediateLeaderBoardBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),25));
        hardLeaderBoardBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),25));

        easyLeaderBoardBtn.getStyleClass().add("buttonsLeaderBordSelector");
        intermediateLeaderBoardBtn.getStyleClass().add("buttonsLeaderBordSelector");
        hardLeaderBoardBtn.getStyleClass().add("buttonsLeaderBordSelector");


        setBottomAnchor(vBox,0.0);
        setTopAnchor(vBox,0.0);
        setRightAnchor(vBox,0.0);

        setLeftAnchor(vBox,0.0);

        setOpacity(0);

    }
    private void buttonStyler(Button button){
        button.getStyleClass().add("generalButton2");
        button.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 15));
    }


    public Button getClearLeaderBordBtn() {
        return clearLeaderBordBtn;
    }

    public Button getEasyItemBtn() {
        return easyItemBtn;
    }

    public Button getIntermediateItemBtn() {
        return intermediateItemBtn;
    }

    public Button getHardItemBtn() {
        return hardItemBtn;
    }


    public Button getEasyLeaderBoardBtn() {
        return easyLeaderBoardBtn;
    }

    public Button getIntermediateLeaderBoardBtn() {
        return intermediateLeaderBoardBtn;
    }

    public Button getHardLeaderBoardBtn() {
        return hardLeaderBoardBtn;
    }

    public Button getGoToMainMenuBtn() {
        return goToMainMenuBtn;
    }


    public ImageView getBackgroundImage() {
        return backgroundImage;
    }

    public Label getFileNotFoundLabel() {
        return fileNotFoundLabel;
    }


}
