package mazemaniac.view.maze;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;

import java.util.Objects;


public class PlayerSprite extends Tile{

    public PlayerSprite() {

        Image sprite = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/Sprites/tile000.png")));
        setFill(new ImagePattern(sprite));
        setStroke(Color.DARKGRAY);

    }

}
