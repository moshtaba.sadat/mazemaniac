package mazemaniac.view.maze;

import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.Objects;

public class MazeView extends AnchorPane {

    private MenuBar menuBar;

    private Menu menuInventory;
    private Button menuInventoryBtn;
    private MenuItem menuItemRules;
    private ImageView imageViewRules;

    private Menu menuExit;
    private Button menuExitBtn;


    private Label fileNotFoundLabel;

    private GridPane gridPane;

    private Label time;

    private Label points;


    private Label coinsCollected;

    private Label mazesCompleted;

    private ImageView mazeBackgroundImage0;
    private ImageView mazeBackgroundImage1;
    private ImageView mazeBackgroundImage2;

    private ImageView mazeBackgroundImage3;

    private StackPane mazeBackgroundImageStackPane;

    private VBox mazeAndTimeVBox;


    private VBox playerStatsVBox;

    private HBox allNodeHBox;

    public MazeView(){
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {

        Image imageRules = new Image(Objects.requireNonNull(
                getClass().getResourceAsStream("/Images/GameRules/Rules (55)_transparent.png")));
        imageViewRules = new ImageView(imageRules);

        menuInventoryBtn = new Button("Inventory");
        menuItemRules = new MenuItem();
        menuItemRules.setGraphic(imageViewRules);
        menuInventory = new Menu(null, menuInventoryBtn, menuItemRules);

        menuExitBtn = new Button("Exit");
        menuExit = new Menu(null,menuExitBtn);

        menuBar = new MenuBar(menuInventory, menuExit);





        fileNotFoundLabel = new Label("File not found, Score will not be saved");

        gridPane = new GridPane();

        time = new Label("Time: 60");
        points = new Label("Point: 1000");
        mazesCompleted = new Label("Labyrinths explored: 0");
        coinsCollected = new Label("Coins collected: 0");

        Image image0 = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/Maze/BackGroundForMaze0.png")));
        Image image1 = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/Maze/BackGroundForMaze1.png")));
        Image image2 = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/Maze/BackGroundForMaze2.png")));
        Image image3 = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/Maze/BackGroundForMaze3.png")));
        mazeBackgroundImage0 = new ImageView(image0);
        mazeBackgroundImage1 = new ImageView(image1);
        mazeBackgroundImage2 = new ImageView(image2);
        mazeBackgroundImage3 = new ImageView(image3);



        mazeBackgroundImageStackPane = new StackPane(mazeBackgroundImage0,mazeBackgroundImage1,
                mazeBackgroundImage2,mazeBackgroundImage3);

        mazeAndTimeVBox = new VBox(time,gridPane);
        playerStatsVBox = new VBox(points,mazesCompleted,coinsCollected);

        allNodeHBox = new HBox(playerStatsVBox,mazeAndTimeVBox);

        getChildren().addAll(mazeBackgroundImageStackPane,allNodeHBox,fileNotFoundLabel,menuBar);
    }

    public void layoutNodes() {

        menuBar.getStyleClass().add("menu-item");

        menuInventory.getStyleClass().add("menu-item");
        menuInventoryBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 20));
        menuInventoryBtn.setStyle("-fx-background-color: transparent; -fx-cursor: hand; -fx-text-fill: white");
        imageViewRules.setStyle("-fx-background-color: transparent; ");
        menuItemRules.getStyleClass().add("menu-item");

        menuExit.getStyleClass().add("menu-item");
        menuExitBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 20));
        menuExitBtn.setStyle(" -fx-background-color: transparent; -fx-cursor: hand; -fx-text-fill: white");

        fileNotFoundLabel.setFont(new Font(40));
        setLeftAnchor(fileNotFoundLabel,200.0);
        setTopAnchor(fileNotFoundLabel,0.0);
        fileNotFoundLabel.setOpacity(0);
        fileNotFoundLabel.setTextFill(Color.RED);

        mazeBackgroundImage1.setOpacity(0);
        mazeBackgroundImage2.setOpacity(0);
        mazeBackgroundImage3.setOpacity(0);

        gridPane.setAlignment(Pos.CENTER);

        setLeftAnchor(allNodeHBox,0.0);
        setBottomAnchor(allNodeHBox,0.0);
        setRightAnchor(allNodeHBox,0.0);
        setTopAnchor(allNodeHBox,0.0);

        mazeAndTimeVBox.setAlignment(Pos.CENTER);
        mazeAndTimeVBox.setSpacing(10);

        playerStatsVBox.setAlignment(Pos.CENTER);
        playerStatsVBox.setSpacing(10);

        allNodeHBox.setAlignment(Pos.CENTER);
        allNodeHBox.setSpacing(30);

        time.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 40));
        points.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),40));
        mazesCompleted.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 40));
        coinsCollected.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"), 40));

        time.setTextFill(Color.WHITE);
        points.setTextFill(Color.WHITE);
        mazesCompleted.setTextFill(Color.WHITE);
        coinsCollected.setTextFill(Color.WHITE);

    }



    public Button getMenuInventoryBtn() {
        return menuInventoryBtn;
    }

    public MenuBar getMenuBar() {
        return menuBar;
    }

    public Button getMenuExitBtn() {
        return menuExitBtn;
    }

    public Label getFileNotFoundLabel() {
        return fileNotFoundLabel;
    }

    public Label getCoinsCollected() {
        return coinsCollected;
    }

    public Label getMazesCompleted() {
        return mazesCompleted;
    }

    public ImageView getMazeBackgroundImage0() {
        return mazeBackgroundImage0;
    }

    public ImageView getMazeBackgroundImage1() {
        return mazeBackgroundImage1;
    }

    public ImageView getMazeBackgroundImage2() {
        return mazeBackgroundImage2;
    }

    public ImageView getMazeBackgroundImage3() {
        return mazeBackgroundImage3;
    }

    public StackPane getMazeBackgroundImageStackPane() {
        return mazeBackgroundImageStackPane;
    }

    public GridPane getGridPane() {
        return gridPane;
    }

    public Label getTime() {
        return time;

    }

    public Label getPoints() {
        return points;
    }



}