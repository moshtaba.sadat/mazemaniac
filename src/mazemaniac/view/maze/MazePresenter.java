package mazemaniac.view.maze;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.gamerover.GamerOverPresenter;
import mazemaniac.view.gamerover.GamerOverView;
import mazemaniac.view.loadingscreen.LoadingScreenPresenter;
import mazemaniac.view.loadingscreen.LoadingScreenView;

import java.util.ArrayList;
import java.util.Optional;

import static mazemaniac.view.SceneUtils.fadeAnimation;
import static mazemaniac.view.SceneUtils.imageFitter;


public class MazePresenter {
    private MazeView view;

    private GameManager model;

    private Timeline timeline;

    private int time;

    private ArrayList<Tile> wall;

    private ArrayList<Tile> path;

    private int indexOfHighlightedFinishPathOfPrevMaze = 0;

    private int pathIndex;
    private int wallIndex;


    public MazePresenter(MazeView view, GameManager model1) {

        this.view = view;
        this.model = model1;

        this.view.getMenuBar().setFocusTraversable(false);
        this.view.getMenuExitBtn().setFocusTraversable(false);
        this.view.getMenuInventoryBtn().setFocusTraversable(false);


        wall = model.getAddngWallSpritesThread().getWallObjects();
        path = model.getAddingPathSpritesThread().getPathObjects();
        try {
            this.model.getFileManager().readFromFile();
        } catch (RuntimeException e) {
            this.view.getFileNotFoundLabel().setOpacity(1);
        }

        model.loadLevel();

        time = 60;
        timeline = updateScoreAndTime();
        timeline.play();


        mazeDrawer(model.getCurrentLevel());
        addEventHandlers();


    }


    public void addEventHandlers() {

        view.getGridPane().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {


                int direction = 0;


                direction = switch (keyEvent.getCode()) {
                    case UP -> 1;
                    case RIGHT -> 2;
                    case DOWN -> 3;
                    case LEFT -> 4;

                    default -> direction;
                };

                int[] prevPlayerPos = model.movePlayer(direction);
                view.getCoinsCollected().setText("Coins collected: " + model.getPlayer().getCoinAmount());

                updateView(prevPlayerPos);


            }
        });

        timeline.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                try {
                    model.getFileManager().addScore(model.getPlayer().getPlayerScore());

                    model.getFileManager().save();
                } catch (RuntimeException e) {
                    view.getFileNotFoundLabel().setOpacity(1);
                    fadeAnimation(view.getFileNotFoundLabel(), 0, 2).play();
                }

                GamerOverView gamerOverView = new GamerOverView();
                new GamerOverPresenter(gamerOverView, model);

                view.getScene().setRoot(gamerOverView);
                Stage stage = (Stage) gamerOverView.getScene().getWindow();
                imageFitter(gamerOverView.getImageViewBackground(), stage);


                stage.setFullScreen(false);
            }
        });

        view.getMenuBar().setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                timeline.pause();

            }
        });

        view.getMenuBar().setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                timeline.play();



                view.fireEvent(new MouseEvent(
                        MouseEvent.MOUSE_CLICKED,
                        0.0,
                        0.0,
                        0.0,
                        0.0,
                        MouseButton.PRIMARY,
                        1,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        true,  // Set synthesized to true to indicate a synthesized event
                        false,
                        false,
                        new PickResult(view, 0.0, 0.0)


                ));
                view.getGridPane().requestFocus();

            }
        });

        view.getMenuExitBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                final Alert Quit = new Alert(Alert.AlertType.CONFIRMATION);

                Quit.setHeaderText("if you leave now your scores won't be saved");
                Quit.setContentText("Press to go back to the main menu");
                Optional<ButtonType> keuze = Quit.showAndWait();

                if (keuze.get().getText().equalsIgnoreCase("CANCEL")) {
                    view.getGridPane().requestFocus();
                    event.consume();
                }
                else{
                    Stage stage = (Stage) view.getScene().getWindow();
                    LoadingScreenView loadingScreenView = new LoadingScreenView();
                    GameManager model = new GameManager();
                    new LoadingScreenPresenter(loadingScreenView,model);

                    view.getScene().setRoot(loadingScreenView);
                    imageFitter(loadingScreenView.getSplashArtImage(),stage);
                }



            }
        });

    }


    private void updateView(int[] prevPlayerPosition) {

        if (model.checkCompletion()) {
            view.getMazesCompleted().setText("Labyrinths explored: " + model.getPlayer().getAmountOfMazeCompletions());

            for (int i = 0; i < view.getMazeBackgroundImageStackPane().getChildren().size(); ++i) {
                int nextIndex = (i + 1) % view.getMazeBackgroundImageStackPane().getChildren().size();
                if (view.getMazeBackgroundImageStackPane().getChildren().get(i).getOpacity() != 0) {

                    view.getMazeBackgroundImageStackPane().getChildren().get(i).setOpacity(0);
                    view.getMazeBackgroundImageStackPane().getChildren().get(nextIndex).setOpacity(1);
                    break;
                }
            }
            mazeDrawer(model.getCurrentLevel());

        } else {
            movePlayerInView(prevPlayerPosition);
        }

    }

    private Timeline updateScoreAndTime() {

        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                time--;
                model.getPlayer().setPoints(-10);
                view.getPoints().setText("Points: " + model.getPlayer().getPoints());
                view.getTime().setText("Time: " + time);

            }
        }));

        timeline.setCycleCount(60);
        return timeline;

    }

    private Tile findCell(int row, int col) {
        for (Node node : view.getGridPane().getChildren()) {
            if (GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == col) {
                return (Tile) node;
            }
        }
        return null;
    }

    private void mazeDrawer(int[][] currentLevel) {
        clearGridPane();

        path.get(indexOfHighlightedFinishPathOfPrevMaze).setFill(Color.TRANSPARENT);

        pathIndex = 0;
        wallIndex = 0;

        int rowFinish = model.getRandomMazegeneration().getStartAndFinish().getFinishCoords()[0];
        int columnFinish = model.getRandomMazegeneration().getStartAndFinish().getFinishCoords()[1];

        drawTiles(currentLevel, rowFinish, columnFinish);
    }

    private void clearGridPane() {
        view.getGridPane().getChildren().clear();
    }

    private void drawTiles(int[][] currentLevel, int rowFinish, int columnFinish) {
        for (int row = 0; row < currentLevel.length; row++) {
            for (int col = 0; col < currentLevel[row].length; col++) {
                Tile newTile = createTile(currentLevel[row][col], col, row, rowFinish, columnFinish);

                if (newTile != null) {
                    view.getGridPane().add(newTile, col, row);
                } else {
                    newTile = new CoinSprite();
                    view.getGridPane().add(newTile, col, row);
                }
            }
        }
    }

    private Tile createTile(int value, int col, int row, int rowFinish, int columnFinish) {
        Tile newTile = new Tile();
        if (value == 0) {
            newTile = createPathTile(col, row, rowFinish, columnFinish);
        } else if (value == 1) {
            newTile = createWallTile();
        } else if (value == 5) {
            newTile = new PlayerSprite();
        } else if (value == 8) {
            newTile = new CoinSprite();
            if (row == rowFinish && col == columnFinish) {
                newTile.setFill(Color.DARKGREEN);
            }
        }

        return newTile;
    }

    private Tile createPathTile(int col, int row, int rowFinish, int columnFinish) {

        if (path != null && pathIndex != path.size() - 1) {

            //give the finish another color
            if (row == rowFinish && col == columnFinish) {
                //use path index and not col because col will increase even if we don't make a path
                path.get(pathIndex).setFill(Color.DARKGREEN);
                //reset the prev maze highlighted finish
                indexOfHighlightedFinishPathOfPrevMaze = pathIndex;

            }

            return path.get(pathIndex++);

        } else {
            PathSprite newTile = new PathSprite();
            if (row == rowFinish && col == columnFinish) {
                newTile.setFill(Color.DARKGREEN);
            }
            return newTile;
        }

    }

    private Tile createWallTile() {
        if (wall != null && wallIndex != wall.size() - 1) {
            return wall.get(wallIndex++);
        } else {
            return new WallSprite();
        }
    }

    private void movePlayerInView(int[] prevPlayerPos) {

        //remove player in gridpane and replace it with a path
        Tile currentPlayerPositionInView = findCell(prevPlayerPos[0], prevPlayerPos[1]);
        view.getGridPane().getChildren().remove(currentPlayerPositionInView);
        Tile newPath = new PathSprite();
        view.getGridPane().add(newPath, prevPlayerPos[1], prevPlayerPos[0]);

        //remove the path in gridpâne where the player will be and replace it with a new playerSprite
        Tile oldPath = findCell(model.getPlayer().getRowX(), model.getPlayer().getColumnY());
        view.getGridPane().getChildren().remove(oldPath);
        Tile newPlayerPosition = new PlayerSprite();
        view.getGridPane().add(newPlayerPosition, model.getPlayer().getColumnY(), model.getPlayer().getRowX());
    }


}