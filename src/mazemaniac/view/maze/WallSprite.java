package mazemaniac.view.maze;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;

import java.util.Objects;

public class WallSprite extends Tile {

    public WallSprite() {
        Image sprite = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/Sprites/WallSprite.png")));
        setFill(new ImagePattern(sprite));
        setStroke(Color.BLACK);

    }
}
