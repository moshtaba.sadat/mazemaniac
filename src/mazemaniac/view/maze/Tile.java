package mazemaniac.view.maze;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Tile extends Rectangle{

    public static final int CELL_SIZE = 17;
    public Tile() {
        setHeight(CELL_SIZE);
        setWidth(CELL_SIZE);
        setFill(Color.TRANSPARENT);
        setStroke(Color.TRANSPARENT);
    }

}
