package mazemaniac.view.maze;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;

import java.util.Objects;

public class CoinSprite extends Tile {

    public CoinSprite() {
        Image sprite = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/Sprites/spinning-coin.png")));
        setFill(new ImagePattern(sprite));

    }
}
