package mazemaniac.view.buffer;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.Objects;

public class BufferView extends AnchorPane {

    private Label nextMazeLbl;

    private ImageView backgoundImage;

    public BufferView(){
        initialiseNodes();
        layoutNodes();

    }

    private void initialiseNodes(){
        nextMazeLbl = new Label("Going Down the dungeon...");

        Image image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/Buffer/Buffer.png")));
        backgoundImage = new ImageView(image);

        getChildren().addAll(backgoundImage,nextMazeLbl);
    }

    private void layoutNodes() {

        nextMazeLbl.setFont(Font.loadFont("file:resources/Font/IMFellDWPicaSC-Regular.ttf", 50));
        nextMazeLbl.setTextFill(Color.WHITE);

        AnchorPane.setTopAnchor(nextMazeLbl, 0.0);
        AnchorPane.setBottomAnchor(nextMazeLbl, 0.0);
        AnchorPane.setLeftAnchor(nextMazeLbl, 0.0);
        AnchorPane.setRightAnchor(nextMazeLbl, 0.0);
    }

    public Label getNextMazeLbl() {
        return nextMazeLbl;
    }

    public ImageView getBackgroundImage() {
        return backgoundImage;
    }
}
