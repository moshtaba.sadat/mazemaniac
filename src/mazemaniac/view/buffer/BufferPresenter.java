package mazemaniac.view.buffer;

import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.maze.MazePresenter;
import mazemaniac.view.maze.MazeView;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.util.Duration;
import static mazemaniac.view.SceneUtils.fadeAnimation;
import static mazemaniac.view.SceneUtils.imageFitter;

public class BufferPresenter {

    private BufferView view;

    private GameManager model;

    private Timeline timeline;



    public BufferPresenter(BufferView view, GameManager model) {
        this.view = view;
        this.model = model;

        this.model.getAddingPathSpritesThread().setDifficulty(this.model.getDifficultySelector());
        this.model.getAddngWallSpritesThread().setDifficulty(this.model.getDifficultySelector());

        timeline = durationOfScene();
        timeline.play();

        addEventHandlers();
        updateView();


    }

    private void addEventHandlers(){
        timeline.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                try {
                    model.getAddingPathSpritesThread().join();
                    model.getAddngWallSpritesThread().join();

                } catch (InterruptedException ignored) {

                }

                MazeView mazeView = new MazeView();
                new MazePresenter(mazeView, model);

                FadeTransition fadeTransition = fadeAnimation(view.getScene().getRoot(),0, 0.3);
                fadeTransition.play();
                fadeTransition.setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {



                        view.getScene().setRoot(mazeView);
                        Stage stage = (Stage) mazeView.getScene().getWindow();

                        imageFitter(mazeView.getMazeBackgroundImage0(),stage);
                        imageFitter(mazeView.getMazeBackgroundImage1(),stage);
                        imageFitter(mazeView.getMazeBackgroundImage2(), stage);
                        imageFitter(mazeView.getMazeBackgroundImage3(),stage);

                        mazeView.getGridPane().requestFocus();
                        if(model.getDifficultySelector().equalsIgnoreCase("hard")){
                            stage.setFullScreen(true);
                        }

                    }
                });

            }
        });

    }

    private void updateView(){

    }

    private Timeline durationOfScene(){
        model.getAddingPathSpritesThread().start();
        model.getAddngWallSpritesThread().start();
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(2), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        view.getNextMazeLbl().setText("You head down into the dungeon...");
                    }
                })
        );
        timeline.setCycleCount(1);
        return timeline;

    }




}
