package mazemaniac.view.nameselector;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.util.Objects;

public class NameSelectorView extends AnchorPane {
    private Button continueBtn;

    private Button goToMainMenuBtn;

    private TextField textField;

    private ImageView backgroundImage;

    private VBox vBox;

    private Label difficultyLbl;

    public NameSelectorView(){
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {

        difficultyLbl = new Label("Difficulty: Easy");

        continueBtn = new Button("Continue");
        goToMainMenuBtn = new Button("Back");

        textField = new TextField();
        textField.setPromptText("Name");
        textField.setMaxWidth(200);



        vBox = new VBox();
        vBox.getChildren().addAll(textField,difficultyLbl,continueBtn);

        Image image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/Images/Name/NameSelectorScreen.png")));
        backgroundImage = new ImageView(image);


        getChildren().addAll(backgroundImage,vBox,goToMainMenuBtn);

    }

    private void layoutNodes() {
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        vBox.setPadding(new Insets(0,0,50,0));



        setLeftAnchor(vBox,100.0);
        setBottomAnchor(vBox,0.0);



        goToMainMenuBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),15));

        textField.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),17));
        difficultyLbl.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),17));
        continueBtn.setFont(Font.loadFont(getClass().getResourceAsStream("/Font/IMFellDWPicaSC-Regular.ttf"),30));


        difficultyLbl.setStyle("-fx-text-fill: white");
        continueBtn.getStyleClass().add("generalButton");


        textField.setPromptText("Name");
        textField.getStyleClass().add("text-field");




        goToMainMenuBtn.setLayoutX(20);
        goToMainMenuBtn.setLayoutY(14);


    }

    public Button getContinueBtn() {
        return continueBtn;
    }

    public Button getGoToMainMenuBtn() {
        return goToMainMenuBtn;
    }


    public TextField getTextfield() {
        return textField;
    }

    public VBox getvBox() {
        return vBox;
    }

    public Label getDifficultyLbl() {
        return difficultyLbl;
    }

    public ImageView getBackgroundImage() {
        return backgroundImage;
    }
}
