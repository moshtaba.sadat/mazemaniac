package mazemaniac.view.nameselector;


import mazemaniac.model.mainmazecreation.GameManager;
import mazemaniac.view.gamerules1.GameRulesPresenter1;
import mazemaniac.view.gamerules1.GameRulesView1;
import mazemaniac.view.mainmenu.MainMenuPresenter;
import mazemaniac.view.mainmenu.MainMenuView;
import javafx.animation.ParallelTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import static mazemaniac.view.SceneUtils.*;


public class NameSelectorPresenter {
    private NameSelectorView view;

    private GameManager model;

    private static final int CHARACTER_LIMIT = 20;

    public NameSelectorPresenter(NameSelectorView view, GameManager model) {
        this.view = view;
        this.model = model;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {

        view.getGoToMainMenuBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                view.getGoToMainMenuBtn().setDisable(true);

                MainMenuView mainMenuView = new MainMenuView();
                new MainMenuPresenter(mainMenuView,model);

                ParallelTransition parallelTransition = new ParallelTransition(
                        fadeAnimation(view.getScene().getRoot(),0,0.3),
                        moveToAnimation(view.getvBox(),+50 ,'y'),
                        moveToAnimation(view.getTextfield(),+100,'y'));
                parallelTransition.play();

                parallelTransition.setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        toMainMenu(view.getGoToMainMenuBtn(),model);
                    }
                });
            }
        });
        hoverAnimation(view.getGoToMainMenuBtn());

        view.getContinueBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String playerName = view.getTextfield().getText();
                if (view.getTextfield().getText().isEmpty() || view.getTextfield().getText().contains(" ")
                        || view.getTextfield().getText().length() >= CHARACTER_LIMIT) {
                    // If it's empty, show an alert or has any spaces
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Please fill in the name field. It must not contain spaces and it must be less" +
                            " than " + CHARACTER_LIMIT + " characters long ");
                    alert.showAndWait();

                    event.consume();

                }
                else {

                    model.getPlayer().setPlayerName(playerName);

                    GameRulesView1 gameRulesView = new GameRulesView1();
                    new GameRulesPresenter1(gameRulesView,model);

                    view.getScene().setRoot(gameRulesView);

                    Stage stage = (Stage) gameRulesView.getScene().getWindow();
                    imageFitter(gameRulesView.getBackgroundImage(), stage);

                }
            }
        });
        hoverAnimation(view.getContinueBtn());
    }

    private void updateView() {
        view.getDifficultyLbl().setText("Difficulty: " + model.getDifficultySelector());
    }


}
