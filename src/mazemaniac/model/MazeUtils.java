package mazemaniac.model;

import java.util.ArrayList;

public class MazeUtils {

    public static boolean isValidSpawn_FrontierCell(int cellRow, int cellColumn, int[][] currentLevel) {
        //checks if the frontier cell does not spawn at the edges
        return cellRow > 0 && cellRow < (currentLevel.length - 1) &&
                cellColumn > 0 && cellColumn < (currentLevel[0].length - 1);
    }

    public static boolean isAlreadyFrontier(int row, int column, ArrayList<int[]> frontierCoordinatesHolder) {
        for (int[] frontier : frontierCoordinatesHolder) {
            if (frontier[0] == row && frontier[1] == column) {
                return true; // Cell is already a frontier
            }
        }
        return false;
    }

    public static boolean isAlreadyPath(int row, int column,  ArrayList<int[]> coordsOfPath) {
        for (int[] isPath : coordsOfPath) {
            if (isPath[0] == row && isPath[1] == column) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidMove(int row, int col,int[][] currentLevel) {
        // Add boundary checks to ensure the move is within the grid
        return row >= 0 && row < currentLevel.length &&
                col >= 0 && col < currentLevel[0].length &&
                currentLevel[row][col] != 1;
    }



}
