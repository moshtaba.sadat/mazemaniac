package mazemaniac.model.mazegenerationusingprims;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import static mazemaniac.model.MazeUtils.*;

public class RandomMazeGeneration {
    private ArrayList<int[]> frontierCoordinatesHolder;
    private ArrayList<int[]> coordsOfPath;
    private int spawnpointRow;
    private int spawnpointColumn;
    private final int[][] frontierCellLooper;
    private final Random random;
    private int[][] currentLevel;
    
    private StartAndFinish startAndFinish;

    private int dimensions;




    public RandomMazeGeneration() {

        this.frontierCoordinatesHolder = new ArrayList<>();
        this.coordsOfPath = new ArrayList<>();

        
        this.startAndFinish = new StartAndFinish();


        this.frontierCellLooper = new int[][]{
                {-2, 0}, {2, 0}, {0, -2}, {0, 2}
        };

        this.random = new Random();



    }

    //not done in constructor because the dimensions need to be set first.
    private void mazeSetup(){

        this.currentLevel = new int[dimensions][dimensions];
        for (int[] ints : currentLevel) {
            Arrays.fill(ints, 1); //fill array with ones (walls)
        }

        spawnpointColumn = random.nextInt(1, currentLevel[0].length - 1);
        spawnpointRow = random.nextInt(1, currentLevel.length - 1);

        spawnCell(spawnpointRow, spawnpointColumn); // Initialize starting point
        calculateFrontiercell(); // Populate frontier cells around the starting point
    }

    public void setDimensions(int dimensions) {
        this.dimensions = dimensions;
        mazeSetup();

    }

    public StartAndFinish getStartAndFinish() {
        return startAndFinish;
    }



    private void spawnCell(int row, int column) {
        //spawns a path at a specified coordinate.
        int isPath = 0;
        currentLevel[row][column] = isPath;
        coordsOfPath.add(new int[]{row, column}); //adds the coordinate to the coordinates of all the paths
    }

    private void calculateFrontiercell() {
        //calculates all the frontiercells of a spawnt cell
        for (int[] i : frontierCellLooper) {
            int rowXfrontier = spawnpointRow + i[0]; //sets rowXfrontier 2 blocks in a direction
            int columnYfrontier = spawnpointColumn + i[1];

            //true if it is a valid frontier cell spawn. it can't be on the edges nor can it be a path and already a frontiercell
            if (isValidSpawn_FrontierCell(rowXfrontier, columnYfrontier,currentLevel)
                    && !isAlreadyFrontier(rowXfrontier, columnYfrontier,frontierCoordinatesHolder)
                    && !isAlreadyPath(rowXfrontier, columnYfrontier,coordsOfPath)) {
                frontierCoordinatesHolder.add(new int[]{rowXfrontier, columnYfrontier});
            }
        }
    }

    private void selectFrontierCel() {
        //select a random frontier cell (= a coordinate)
        int randomIndex = random.nextInt(frontierCoordinatesHolder.size());
        int[] selectedArray = frontierCoordinatesHolder.get(randomIndex);
        
        spawnpointRow = selectedArray[0];
        spawnpointColumn = selectedArray[1];
        //spawn die frontier cell
        spawnCell(spawnpointRow, spawnpointColumn);

        //calculate the distance between all the paths to the just spawned frontiercel
        double shortestDistance;
        int rowForDistance = 0;
        int columnForDistance = 0;
        double prevDistance = Double.MAX_EXPONENT;
        for (int[] ints : coordsOfPath) { //loop through all the paths which includes the just spawned frontiercel
            if (ints[0] == spawnpointRow && ints[1] == spawnpointColumn) {
                continue; //skip that frontiercell
            }
            //use the formula of distance between two points to calculate the distance between them
            shortestDistance = Math.sqrt(Math.pow(ints[0] - spawnpointRow, 2) + Math.pow(ints[1] - spawnpointColumn, 2));

            if (shortestDistance < prevDistance) {
                prevDistance = shortestDistance;
                rowForDistance = ints[0];
                columnForDistance = ints[1];
            }
        }

        //spawn in the middle of the closest cell tot the just spawned frontier cell a path.
        // by using the formula of the middle between two points
        spawnCell((rowForDistance + spawnpointRow) / 2, (columnForDistance + spawnpointColumn) / 2);
        frontierCoordinatesHolder.remove(randomIndex);
        //remove that just spawned frontier cel
    }


    public int[][] GenerateMaze() {
        while (!frontierCoordinatesHolder.isEmpty()) {
            selectFrontierCel();
            calculateFrontiercell();
        }
        startAndFinish.outerIndexes(currentLevel);
        startAndFinish.spawnStart(currentLevel);
        startAndFinish.spawnFinish(currentLevel);

        return currentLevel;
    }



}


