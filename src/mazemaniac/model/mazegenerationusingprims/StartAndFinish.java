package mazemaniac.model.mazegenerationusingprims;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class StartAndFinish {
    private ArrayList<int[]> StartCellCoordinates;
    private int[] randomCoordsOfEdgeStart;
    private int[] startingCoords;
    private int[] finishCoords;
    private final Random random;
    private final int[][] directions;




    public StartAndFinish() {
        this.StartCellCoordinates = new ArrayList<>();

        this.randomCoordsOfEdgeStart = new int[2];
        this.startingCoords = new int[2];
        this.finishCoords = new int[2];

        this.directions = new int[][]{
                {1, 0}, {0, 1}, {-1, 0}, {0, -1}
        };

        this.random = new Random();
    }

    public int[] getStartingCoords() {
        return startingCoords;
    }

    public int[] getFinishCoords() {
        return finishCoords;
    }

    public void outerIndexes(int[][] currentLevel) {
        //store outer edges except corners in the List to later randomly pick a starting cell coordinate
        for (int i = 0; i < currentLevel.length; ++i) {
            if (i == 0 || i == currentLevel.length - 1) {
                continue;
            }
            //top and bottom row
            StartCellCoordinates.add(new int[]{0, i});
            StartCellCoordinates.add(new int[]{currentLevel.length - 1, i});

            //right and left columns
            StartCellCoordinates.add(new int[]{i, 0});
            StartCellCoordinates.add(new int[]{i, currentLevel.length - 1});
        }
    }
    public void spawnStart(int[][] currentLevel) {
        //pick a random starting point
        int randomIndex = random.nextInt(0, StartCellCoordinates.size());
        randomCoordsOfEdgeStart = StartCellCoordinates.get(randomIndex);

        //store that starting point to later set the player position
        startingCoords[0] = randomCoordsOfEdgeStart[0];
        startingCoords[1] = randomCoordsOfEdgeStart[1];

        //make a copy of the array otherwise it will apparently give it by reference and not by value
        int[] randomCoordsOfEdgeStartCopy = Arrays.copyOf(randomCoordsOfEdgeStart,randomCoordsOfEdgeStart.length);

        //if the column of the starting position equals the last column i.e. the right most column
        if(randomCoordsOfEdgeStartCopy[1] == currentLevel.length - 1){
            den_Driller(1,randomCoordsOfEdgeStartCopy ,
                    3,currentLevel);
        }
        else if(randomCoordsOfEdgeStartCopy[1] == 0){ // left most column
            den_Driller(3,randomCoordsOfEdgeStartCopy ,
                    1,currentLevel);
        }
        else if (randomCoordsOfEdgeStartCopy[0] == 0) { // start drilling at first row
            den_Driller(2,randomCoordsOfEdgeStartCopy ,
                    0,currentLevel);
        }
        else if (randomCoordsOfEdgeStartCopy[0] == currentLevel.length - 1) { //last row
            den_Driller(0,randomCoordsOfEdgeStartCopy ,
                    2,currentLevel);
        }



    }

    public void spawnFinish(int[][] currentLevel){


        int randomRowOrColumn = random.nextInt(1,currentLevel.length-1);
        int[] randomCoordsOfEdgeStartCopy;

        //if the row of the starting coordinates is 0 (=firs row) then the reverse factor of the row is its max amount
        //of rows.
        if(randomCoordsOfEdgeStart[0] == 0){
            //the second argument will then pick a random column in the first row
            calculateOppositeEdge(currentLevel.length - 1,randomRowOrColumn,randomCoordsOfEdgeStart);
            randomCoordsOfEdgeStartCopy = Arrays.copyOf(randomCoordsOfEdgeStart, randomCoordsOfEdgeStart.length);
            den_Driller(0,randomCoordsOfEdgeStartCopy, 2,currentLevel); //laatse rij
        }

        //row of starting coordinates is the last row so its reverse factor is 0 (=first row)
        else if(randomCoordsOfEdgeStart[0] == currentLevel.length -1 ){
            calculateOppositeEdge(0,randomRowOrColumn,randomCoordsOfEdgeStart);
            randomCoordsOfEdgeStartCopy = Arrays.copyOf(randomCoordsOfEdgeStart, randomCoordsOfEdgeStart.length);
            den_Driller(2,randomCoordsOfEdgeStartCopy, 0,currentLevel); //eerste rij
        }

        //the starting column is the left most. so the reverse factor is the right most
        else if(randomCoordsOfEdgeStart[1] == 0){
            //the second argument will then pick a random row in the left most column
            calculateOppositeEdge(randomRowOrColumn,currentLevel.length-1,randomCoordsOfEdgeStart);
            randomCoordsOfEdgeStartCopy = Arrays.copyOf(randomCoordsOfEdgeStart, randomCoordsOfEdgeStart.length);
            den_Driller(1,randomCoordsOfEdgeStartCopy, 3,currentLevel); //rechtse kolom

        }

        else if (randomCoordsOfEdgeStart[1] == currentLevel.length - 1) {
            calculateOppositeEdge(randomRowOrColumn,0,randomCoordsOfEdgeStart);
            randomCoordsOfEdgeStartCopy = Arrays.copyOf(randomCoordsOfEdgeStart, randomCoordsOfEdgeStart.length);
            den_Driller(3,randomCoordsOfEdgeStartCopy, 1,currentLevel); // linkse kolom
        }

        //store the position of the finish to later set as maze completion coords
        finishCoords[0] = randomCoordsOfEdgeStart[0];
        finishCoords[1] = randomCoordsOfEdgeStart[1];

    }

    //drill a straight pad based on a direction and the side the spawn point or finish point is.
    private void den_Driller(int skipDirection, int[] coordOfStartOrFinish, int drillDirection, int[][] currentLevel) {

        int isPath = 0;
        currentLevel[coordOfStartOrFinish[0]][coordOfStartOrFinish[1]] = isPath; //place a path at the start or finish

        while(true){
            //move one block up, down, right or left based one the drill direction
            coordOfStartOrFinish[0] += directions[drillDirection][0];
            coordOfStartOrFinish[1] += directions[drillDirection][1];

            //if the block you moved to is zero than you have connected the finish or start to the rest of the maze
            if(currentLevel[coordOfStartOrFinish[0]][coordOfStartOrFinish[1]] == isPath){
                return;
            }

            //make the block you moved to a path lets call this original position
            else currentLevel[coordOfStartOrFinish[0]][coordOfStartOrFinish[1]] = isPath;


            int currentRow = coordOfStartOrFinish[0];
            int currentColumn = coordOfStartOrFinish[1];

            //from the original position check all the directions except the direction you came from
            //since this will always be zero
            for(int i = 0; i < directions.length; ++i){
                if(i == skipDirection){
                    continue;
                }

                coordOfStartOrFinish[0] += directions[i][0];
                coordOfStartOrFinish[1] += directions[i][1];

                //if you found a zero then you have connected the finish or start to the rest of the maze
                if(currentLevel[coordOfStartOrFinish[0]][coordOfStartOrFinish[1]] == isPath){
                    return;
                }
                else{
                    //if no zero was found then go back to your original position
                    coordOfStartOrFinish[0] = currentRow;
                    coordOfStartOrFinish[1] = currentColumn;
                }
            }
        }
    }

    private void calculateOppositeEdge(int reverseFactorRow, int reverseFactorColumn, int[] EdgeCoordinates){
        EdgeCoordinates[0] = reverseFactorRow;
        EdgeCoordinates[1] = reverseFactorColumn;
    }





}
