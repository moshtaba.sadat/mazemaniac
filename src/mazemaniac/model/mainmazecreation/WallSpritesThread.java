package mazemaniac.model.mainmazecreation;

import mazemaniac.view.maze.Tile;
import mazemaniac.view.maze.WallSprite;
import java.util.ArrayList;

public class WallSpritesThread extends Thread{

    private ArrayList<Tile> wallObjects;
    private String difficulty;
    public WallSpritesThread() {
        this.wallObjects = new ArrayList<>();
    }

    @Override
    public void run(){
        int amountOfObjects;
        if(difficulty.equalsIgnoreCase("intermediate")){ //30x30
            amountOfObjects = 506;
        } else if (difficulty.equalsIgnoreCase("hard")) { //50x50
            amountOfObjects = 1346;

        }else {
            amountOfObjects = 236; //20x20
        }

        for(int i = 0; i < amountOfObjects; ++i){
            wallObjects.add(new WallSprite());
        }
    }

    public ArrayList<Tile> getWallObjects() {
        return wallObjects;
    }
    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }
}


/*
The amount of objects is not randomly determined. I conducted a data analysis
on the maze generation algorithm to determine Max number of walls and paths
created for an X-dimensional maze that could occur. To achieve this, I iterated through the
generated maze, counting the walls (represented by 1) and paths (represented by 0)
using a simple algorithm. This process was repeated 2000 times, and all the
values were recorded in a text file. Subsequently, I identified all unique
instances of occurring quantities of walls and paths. This was straightforward
since there were only three unique instances of these quantities, differing
by one or two. Please refer to the "wallAndPathEasy.txt" file for the
distribution of walls and paths across 2000 randomly 20x20 generated mazes.
 */