package mazemaniac.model.mainmazecreation;

public class PlayerScore implements Comparable<PlayerScore> {

    private String name;


    private int points;

    private int mazesSolved;

    private int coinsCollected;

    public PlayerScore(String name,  int score, int mazesSolved, int coinsCollected) {
        this.name = name;
        this.points = score;
        this.mazesSolved = mazesSolved;
        this.coinsCollected = coinsCollected;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public int getMazesSolved() {
        return mazesSolved;
    }

    public int getCoinsCollected() {
        return coinsCollected;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setPoints(int points) {
        this.points = points;
    }

    public void setMazesSolved(int mazesSolved) {
        this.mazesSolved = mazesSolved;
    }

    public void setCoinsCollected(int coinsCollected) {
        this.coinsCollected = coinsCollected;
    }

    @Override
    public int compareTo(PlayerScore o) {
        return o.points - this.points;
    }
}
