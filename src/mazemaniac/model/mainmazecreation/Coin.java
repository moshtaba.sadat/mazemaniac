package mazemaniac.model.mainmazecreation;

import java.util.Random;

public class Coin {

    private int coinCollectionCount = 0;



    //spawn a random coin in the maze on a spot that is not a wall(1) nor on the position of the player
    public void coinSpawn(int[][] currentLevel, int prowX, int pcolumnY) {

        Random random = new Random();

        int coinRowX = random.nextInt(0, currentLevel.length);
        int coinColumnY = random.nextInt(0, currentLevel[coinRowX].length);

        while (true) {
            if (currentLevel[coinRowX][coinColumnY] != 1 && coinColumnY != pcolumnY && coinRowX != prowX) {
                currentLevel[coinRowX][coinColumnY] = 8;
                break;
            } else {
                coinRowX = random.nextInt(0,currentLevel.length);
                coinColumnY = random.nextInt(0, currentLevel[coinRowX].length);
            }
        }
    }

    public int getCoinCollectionCount() {
        return coinCollectionCount;
    }

    public void setCoinCollectionCount(int coinCollectionCount) {
        this.coinCollectionCount = coinCollectionCount;
    }
}
