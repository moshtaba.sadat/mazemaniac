package mazemaniac.model.mainmazecreation;

import mazemaniac.model.mazegenerationusingprims.RandomMazeGeneration;

import static mazemaniac.model.MazeUtils.isValidMove;

public class GameManager {

    private Player player;

    private RandomMazeGeneration randomMazegeneration;

    private int[][] currentLevel;

    private String difficultySelector  = "easy";

    private Coin coin;


    private FileManager fileManager;


    private PathSpritesThread addingPathSpritesThread;

    private WallSpritesThread addngWallSpritesThread;







    public GameManager() {


        this.addingPathSpritesThread = new PathSpritesThread();
        this.addngWallSpritesThread  = new WallSpritesThread();
        this.player = new Player();
        this.coin = new Coin();
        this.fileManager = new FileManager();

    }

    public RandomMazeGeneration getRandomMazegeneration() {
        return randomMazegeneration;
    }

    public PathSpritesThread getAddingPathSpritesThread() {
        return addingPathSpritesThread;
    }

    public WallSpritesThread getAddngWallSpritesThread() {
        return addngWallSpritesThread;
    }

    public FileManager getFileManager() {
        return fileManager;
    }

    public int[][] getCurrentLevel() {
        return currentLevel;
    }


    public void setDifficultySelector(String difficultySelector) {
        this.difficultySelector = difficultySelector;
        fileManager.setDifficulty(this.difficultySelector);
    }



    public String getDifficultySelector() {
        return difficultySelector;
    }

    public Player getPlayer() {
        return player;
    }



    public void loadLevel(){

        int amountOfCoins = 8;

        randomMazegeneration = new RandomMazeGeneration();

        if (difficultySelector.equalsIgnoreCase("easy")) {
            randomMazegeneration.setDimensions(20);
        } else if (difficultySelector.equalsIgnoreCase("Intermediate")) {
            randomMazegeneration.setDimensions(30);
            amountOfCoins = 12;
        } else if (difficultySelector.equalsIgnoreCase("hard")) {
            randomMazegeneration.setDimensions(50);
            amountOfCoins = 20;

        }



        loadCustomLevel(amountOfCoins);


    }

    public void loadCustomLevel(int amountOfCoins){
        this.currentLevel = randomMazegeneration.GenerateMaze();
        setPlayerPosition(randomMazegeneration.getStartAndFinish().getStartingCoords()[0],
                randomMazegeneration.getStartAndFinish().getStartingCoords()[1],currentLevel);
        while(amountOfCoins != 0){
            spawnInitialCoins();
            amountOfCoins--;
        }

    }


    private void setPlayerPosition(int row,int column, int[][] currentLevel) {

        player.setPosition(row,column);

        currentLevel[player.getRowX()][player.getColumnY()] = 5;

    }
    private void spawnInitialCoins() {
        coin.coinSpawn(currentLevel, player.getRowX(), player.getColumnY());
    }

    public int[] movePlayer(int direction) {

        int tempX = player.getRowX();
        int tempY = player.getColumnY();

        int newX = tempX;
        int newY = tempY;

        if (direction == 1) { //up
            newX = tempX - 1;
        } else if (direction == 2) { //right
            newY = tempY + 1;
        } else if (direction == 3) { //down
            newX = tempX + 1;
        } else if (direction == 4) { //left
            newY = tempY - 1;
        }

        if (isValidMove(newX,newY,currentLevel)) {

            player.setPosition(newX, newY);
            if(checkCoinCollection(newX,newY)){
                player.collectCoin();
                coin.setCoinCollectionCount(coin.getCoinCollectionCount() + 1);
                player.setPoints(50);
            }
            currentLevel[tempX][tempY] = 0;
            currentLevel[newX][newY] = 5;

        }

        return new int[]{tempX,tempY};

    }

    private boolean checkCoinCollection(int newX, int newY){
        return currentLevel[newX][newY] == 8;
    }

    public boolean checkCompletion() {

            int row = randomMazegeneration.getStartAndFinish().getFinishCoords()[0];
            int column = randomMazegeneration.getStartAndFinish().getFinishCoords()[1];

            if(player.getRowX() == row && player.getColumnY() == column){
                    player.setPoints(100);
                    player.addAmountOfMazesSolved();
                    loadLevel();

                return true;
            }
            return false;

    }






}
