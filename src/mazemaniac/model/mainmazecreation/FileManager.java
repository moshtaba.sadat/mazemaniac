package mazemaniac.model.mainmazecreation;

import java.io.*;
import java.util.*;

public class FileManager {


    private String filename;

    private String difficulty = "easy";

    private List<PlayerScore> playerScores;


    private static final String HIGHSCORES_PATH  = "HighScores/";


    public FileManager() {
        playerScores = new ArrayList<>();
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
        filename = fileSelector();
    }

    public List<PlayerScore> getPlayerScores() {
        return playerScores;
    }

    public void addScore(PlayerScore aPlayersScore) {
        playerScores.add(aPlayersScore);
    }

    public String fileSelector(){
        if(difficulty.equalsIgnoreCase("easy")){
            return "HighScoresEasy.txt";
        } else if (difficulty.equalsIgnoreCase("Intermediate")) {
            return "HighScoresIntermediate.txt";
        } else if (difficulty.equalsIgnoreCase("Hard")) {
            return "HighScoresHard.txt";
        }
        return "HighScoresEasy.txt";
    }

    public void sort(){
        Collections.sort(playerScores);
    }

    public void save() {

        sort();

        filename = fileSelector();

        try (BufferedWriter writer = new BufferedWriter(new FileWriter( HIGHSCORES_PATH + filename, true))) {

            for (PlayerScore score : playerScores) {

                writer.write(String.format("%s %d %d %d\n", score.getName(), score.getPoints(),
                        score.getMazesSolved(), score.getCoinsCollected()));


            }

        }
        catch(IOException e){
            throw new RuntimeException("Error saving data to file: " +  HIGHSCORES_PATH + filename, e);
        }



    }

    public void readFromFile() {

        filename = fileSelector();



        try (BufferedReader reader = new BufferedReader(new FileReader( HIGHSCORES_PATH + filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                // go through the file line by line and split it based on spaces in the line
                String[] parts = line.split(" ");
                if (parts.length == 4) {

                    String name = parts[0];
                    int score = Integer.parseInt(parts[1]);
                    int mazesSolved = Integer.parseInt(parts[2]);
                    int coinsCollected = Integer.parseInt(parts[3]);
                    addScore(new PlayerScore(name, score, mazesSolved, coinsCollected));
                    if(playerScores.size() == 20){
                        break;
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Error saving data to file: " + HIGHSCORES_PATH + filename, e);
        }
        clearLeaderBoard();

    }

    public void clearLeaderBoard() {
        filename = fileSelector();

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(HIGHSCORES_PATH + filename))) {
            // Clear the leaderboard by writing an empty string
            writer.write("");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }




}
