package mazemaniac.model.mainmazecreation;

public class Player {


    private int pRowX;
    private int pColumnY;

    private PlayerScore playerScore;

    public Player() {
        playerScore = new PlayerScore("Default", 1000,0,0);
    }





    public void  setPosition(int row, int column) {
        this.pRowX = row;
        this.pColumnY = column;
    }
    public int getRowX() {
        return pRowX;
    }
    public int getColumnY() {
        return pColumnY;
    }


    public int getAmountOfMazeCompletions() {
        return playerScore.getMazesSolved();
    }
    public void addAmountOfMazesSolved() {
        playerScore.setMazesSolved(playerScore.getMazesSolved() + 1);
    }


    public int getPoints() {
        return playerScore.getPoints();
    }
    public void setPoints(int points) {
       playerScore.setPoints(playerScore.getPoints() + points);
    }


    public String getPlayerName() {
        return playerScore.getName();
    }
    public void setPlayerName(String playerName) {
        playerScore.setName(playerName);
    }


    public void collectCoin() {
        playerScore.setCoinsCollected(playerScore.getCoinsCollected() + 1);
    }
    public int getCoinAmount() {
        return playerScore.getCoinsCollected();
    }

    public PlayerScore getPlayerScore() {
        return playerScore;
    }
}
